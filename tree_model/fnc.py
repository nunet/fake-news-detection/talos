import ngram
import pandas as pd
import numpy as np
import cPickle
from helpers import *
from CountFeatureGenerator import *
from TfidfFeatureGenerator import *
from SvdFeatureGenerator import *
from Word2VecFeatureGenerator import *
from SentimentFeatureGenerator import *

import sys
from itertools import chain
from collections import Counter
from sklearn.model_selection import StratifiedKFold, GroupKFold
import xgboost as xgb
from collections import Counter
from SvdFeatureGenerator import *
from AlignmentFeatureGenerator import *
from score import *

import grpc
from concurrent import futures
import time

from scipy import sparse

#from AlignmentFeatureGenerator import *

sys.path.append("./service_spec")
import fnc_stance_detection_pb2 as pb2
import fnc_stance_detection_pb2_grpc as pb2_grpc

params_xgb = {
	
    'max_depth': 6,
    'colsample_bytree': 0.6,
    'subsample': 1.0,
    'eta': 0.1,
    'silent': 1,
    #'objective': 'multi:softmax',
    'objective': 'multi:softprob',
    'eval_metric':'mlogloss',
    'num_class': 4
}

num_round = 1000

		
class GRPCapi(pb2_grpc.FNCStanceDetectionServicer):
    def stance_detection(self, req, ctxt):
        headline = req.headline
        body = req.body
        input = {'headline' : headline, 'body' : body}
	process(headline, body)
        df_output = train(headline, body)
	stance_pred = pb2.Stance()
	stance_pred.stance_class = str(df_output['Stance'])
	stance_pred.pred_agree = df_output['prob_0']
	stance_pred.pred_disagree = df_output['prob_1']
	stance_pred.pred_discuss = df_output['prob_2']
	stance_pred.pred_unrelated = df_output['prob_3']
        return stance_pred

def process(headline, body):
		
    read = False
    if not read:
	body_train = pd.read_csv("train_bodies_processed.csv", encoding='utf-8')
	stances_train = pd.read_csv("train_stances_processed.csv", encoding='utf-8')
	# training set
	train = pd.merge(stances_train, body_train, how='left', on='Body ID')
	targets = ['agree', 'disagree', 'discuss', 'unrelated']
	targets_dict = dict(zip(targets, range(len(targets))))
	train['target'] = map(lambda x: targets_dict[x], train['Stance'])
	print 'train.shape:'
	print train.shape
	n_train = train.shape[0]

	data = train
	# read test set, no 'Stance' column in test set -> target = NULL
	# concatenate training and test set
	test_flag = True
	if test_flag:
	    #body_test = pd.read_csv(body, encoding='utf-8')
	    #headline_test = pd.read_csv(headline, encoding='utf-8')
	    test = pd.DataFrame([[headline, body]], columns=["Headline","articleBody"])            
	    #test = pd.merge(headline_test, body_test, how="left", on="Body ID")
			
	    data = pd.concat((train, test)) # target = NaN for test set
	    print data
	    print 'data.shape:'
	    print data.shape

	    train = data[~data['target'].isnull()]
	    print train
	    print 'train.shape:'
	    print train.shape
			
	    test = data[data['target'].isnull()]
	    print test
	    print 'test.shape:'
	    print test.shape
		
	#data = data.iloc[:100, :]
		
	#return 1
		
	print "generate unigram"
	data["Headline_unigram"] = data["Headline"].map(lambda x: preprocess_data(x))
	data["articleBody_unigram"] = data["articleBody"].map(lambda x: preprocess_data(x))

	print "generate bigram"
	join_str = "_"
	data["Headline_bigram"] = data["Headline_unigram"].map(lambda x: ngram.getBigram(x, join_str))
	data["articleBody_bigram"] = data["articleBody_unigram"].map(lambda x: ngram.getBigram(x, join_str))
		
	print "generate trigram"
	join_str = "_"
	data["Headline_trigram"] = data["Headline_unigram"].map(lambda x: ngram.getTrigram(x, join_str))
	data["articleBody_trigram"] = data["articleBody_unigram"].map(lambda x: ngram.getTrigram(x, join_str))
		
	with open('data.pkl', 'wb') as outfile:
	    cPickle.dump(data, outfile, -1)
	    print 'dataframe saved in data.pkl'

    else:
	with open('data.pkl', 'rb') as infile:
	    data = cPickle.load(infile)
	    print 'data loaded'
	    print 'data.shape:'
	    print data.shape
    #return 1

    # define feature generators
    countFG    = CountFeatureGenerator()
    tfidfFG    = TfidfFeatureGenerator()
    svdFG      = SvdFeatureGenerator()
    word2vecFG = Word2VecFeatureGenerator()
    sentiFG    = SentimentFeatureGenerator()
    #walignFG   = AlignmentFeatureGenerator()
    generators = [countFG, tfidfFG, svdFG, word2vecFG, sentiFG]
    #generators = [svdFG, word2vecFG, sentiFG]
    #generators = [tfidfFG]
    #generators = [countFG]
    #generators = [walignFG]

    for g in generators:
	g.process(data)

    for g in generators:
	g.read('train')

    #for g in generators:
    #    g.read('test')

    print 'done'
	
def build_data():
    
    # create target variable
    body = pd.read_csv("train_bodies.csv")
    stances = pd.read_csv("train_stances.csv")
    data = pd.merge(stances, body, how='left', on='Body ID')
    targets = ['agree', 'disagree', 'discuss', 'unrelated']
    targets_dict = dict(zip(targets, range(len(targets))))
    data['target'] = map(lambda x: targets_dict[x], data['Stance'])
		    
    data_y = data['target'].values

    # read features
    generators = [
            CountFeatureGenerator(),
            TfidfFeatureGenerator(),
            SvdFeatureGenerator(),
            Word2VecFeatureGenerator(),
            SentimentFeatureGenerator()
            #AlignmentFeatureGenerator()
            ]

    features = [f for g in generators for f in g.read('train')]

    #data_x = np.hstack(features)
    data_x = sparse.hstack(features)#data_x = np.hstack(features)
    #print data_x[0,:]
    #print 'data_x.shape'
    #print data_x.shape
    #print 'data_y.shape'
    #print data_y.shape
    
    #with open('data_new.pkl', 'wb') as outfile:
    #    cPickle.dump(data_x, outfile, -1)
    #    print 'data saved in data_new.pkl'

    return data_x, data_y


def build_test_data(headline, body):
    
    # create target variable
    # replace file names when test data is ready
    #body = pd.read_csv("test_data.csv")
    #stances = pd.read_csv("test_stance.csv") # needs to contain pair id
    #data = pd.merge(stances, body, how='left', on='Body ID')
    data = pd.DataFrame([[headline, body]], columns=["Headline","articleBody"])    

    # read features
    generators = [
            CountFeatureGenerator(),
	    TfidfFeatureGenerator(),
	    SvdFeatureGenerator(),
	    Word2VecFeatureGenerator(),
	    SentimentFeatureGenerator()
	    ]

    features = [f for g in generators for f in g.read("test")]
    print len(features)
    #return 1

    data_x = sparse.hstack(features)
    #print data_x[0,:]
    #print 'test data_x.shape'
    #print data_x.shape
    #print data['Body ID'].values.shape
    # pair id
    return data_x

def perfect_score(truth_y):
    
    score = 0
    for i in range(truth_y.shape[0]):
        if truth_y[i] == 3: score += 0.25
        else: score += 1

    return score

def eval_metric(yhat, dtrain):
    y = dtrain.get_label()
    yhat = np.argmax(yhat, axis=1)
    predicted = [LABELS[int(a)] for a in yhat]
    actual = [LABELS[int(a)] for a in y]
    s, _ = score_submission(actual, predicted)
    s_perf, _ = score_submission(actual, actual)
    score = float(s) / s_perf
    return 'score', score

def train(headline, body):
	
    data_x, data_y = build_data()
    # read test data
    test_x = build_test_data(headline, body)   

    w = np.array([1 if y == 3 else 4 for y in data_y])
    print 'w:'
    print w
    print np.mean(w)

    n_iters = 500
    #n_iters = 50
    # perfect score on training set
    print 'perfect_score: ', perfect_score(data_y)
    print Counter(data_y)

    dtrain = xgb.DMatrix(data_x, label=data_y, weight=w)
    dtest = xgb.DMatrix(test_x)
    watchlist = [(dtrain, 'train')]
    bst = xgb.train(params_xgb, 
		    dtrain,
		    n_iters,
		    watchlist,
		    feval=eval_metric,
                    verbose_eval=10)

    #pred_y = bst.predict(dtest) # output: label, not probabilities
    #pred_y = bst.predict(dtrain) # output: label, not probabilities
    pred_prob_y = bst.predict(dtest).reshape(test_x.shape[0], 4) # predicted probabilities
    pred_y = np.argmax(pred_prob_y, axis=1)
    print 'pred_y.shape:'
    print pred_y.shape
    predicted = [LABELS[int(a)] for a in pred_y]
    #print predicted

    # save (id, predicted and probabilities) to csv, for model averaging
    #stances = pd.read_csv("test_stance.csv") # same row order as predicted
    stances = pd.DataFrame([[headline, body]], columns=["Headline","articleBody"])	
		
    df_output = pd.DataFrame()
    df_output['Headline'] = stances['Headline']
    #df_output['Body ID'] = stances['Body ID']
    df_output['Stance'] = predicted
    df_output['prob_0'] = pred_prob_y[:, 0]
    df_output['prob_1'] = pred_prob_y[:, 1]
    df_output['prob_2'] = pred_prob_y[:, 2]
    df_output['prob_3'] = pred_prob_y[:, 3]
    #df_output.to_csv('submission.csv', index=False)
    df_output.to_csv('tree_pred_prob_cor2.csv', index=False)
    df_output[['Headline','Stance']].to_csv('tree_pred_cor2.csv', index=False)

    print df_output
    print Counter(df_output['Stance'])
    return df_output
    #pred_train = bst.predict(dtrain).reshape(data_x.shape[0], 4)
    #pred_t = np.argmax(pred_train, axis=1)
    #predicted_t = [LABELS[int(a)] for a in pred_t]
    #print Counter(predicted_t)

#def fnc_grpc():	
if __name__ == "__main__":
    grpc_port = 9090
    grpc_server = grpc.server(futures.ThreadPoolExecutor(max_workers=10))
    pb2_grpc.add_FNCStanceDetectionServicer_to_server(GRPCapi(), grpc_server)
    grpc_server.add_insecure_port('[::]:' + str(grpc_port))
    grpc_server.start()
    print("GRPC Server Started on port: " + str(grpc_port))
		
    try:
        while True:
	    time.sleep(10)
    except KeyboardInterrupt:
        print("Exiting....")
        grpc_server.stop(0)
	
