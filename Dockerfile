FROM ubuntu:18.04

RUN apt update; apt upgrade -y

RUN apt install -y vim \
                  git \
                  wget \
                  gfortran \
                  liblapack-dev \
                  libopenblas-dev \
                  python-dev \
                  python-scipy \
                  python-minimal \
                  python-nose

RUN wget https://bootstrap.pypa.io/get-pip.py
RUN python2 get-pip.py

COPY . talos/
WORKDIR talos/

# Copying word2vec model trained on Google News corpus
RUN wget https://gitlab.com/nunet/fake-news-detection/data-storage/-/raw/master/GoogleNews-vectors-negative300.bin.gz

RUN python2 -m pip install -r requirements.txt

# Copying talos Stance Detection dataset for FNC-1
RUN git clone https://github.com/FakeNewsChallenge/fnc-1.git
RUN cp fnc-1/* tree_model/
RUN cp fnc-1/* deep_learning_model/
RUN rm -r fnc-1

RUN python2 nltk_downloader.py

RUN cp tree_model/test_stances_unlabeled.csv tree_model/test_stances_unlabeled_processed.csv

WORKDIR tree_model/service_spec
RUN python -m grpc_tools.protoc -I. --python_out=. --grpc_python_out=. fnc_stance_detection.proto

WORKDIR /talos/tree_model