# CISCO Talos stance detection service

[Original repository](https://github.com/Cisco-Talos/fnc-1)

# gRPC-based Service
Running grpc server

        docker build -t image_name .
        docker run -it image_name /bin/bash
        python fnc.py

Running example grpc client

        python grpc_test_client.py

<!--
  Copyright 2017 Cisco Systems, Inc.
 
  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at
 
    http://www.apache.org/licenses/LICENSE-2.0
 
  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
-->

Due to complexity to isolate prediction from training with this model the adaptation of this code is left in favor of other Fake News Challange winners (see related [closing issue](https://gitlab.com/nunet/fake-news-detection/talos/-/issues/3)).

